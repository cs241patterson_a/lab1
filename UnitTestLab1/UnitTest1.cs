﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Lab1Lib;
using System.IO;

namespace UnitTestLab1
{
    [TestClass]
    public class UnitTest1
    {
        // Test - Create an instance of Student
        [TestMethod]
        public void CreateDefaultStudent()
        {
            // Create a new instance of class "Student" using the default
            // constructor.
            // In C# we must always use "new" to create a new instance of a 
            // class.
            Student student = new Student();

            // The default constructor should set the property FirstName
            // to "Unknown"
            Assert.AreEqual("Unknown", student.FirstName);
        }

        // Test - Create an instance of Class
        [TestMethod]
        public void CreateDefaultClass()
        {
            // Create an instance of class "Class"
            Class myClass = new Class();

            // The default constructor for "Class" sets the property
            // NStudents to 0.
            Assert.AreEqual(0, myClass.NStudents);
        }

        // Test - Create an instance of student with first and last nxame
        [TestMethod]
        public void CreateNamedStudent()
        {
            string firstName = "Joe";
            Student student = new Student(firstName, "Cool");
            Assert.AreEqual("Joe", student.FirstName);
            Assert.AreEqual("Cool", student.LastName);
        }

        [TestMethod]
        public void GetDescription()
        {
            Student student = new Student("Joe", "Cool");
            Assert.AreEqual("Joe Cool is a student.", student.GetDescription());
        }

        [TestMethod]
        public void GetCSDescription()
        {
            Student student = new CSStudent("Joe", "Cool");
            Assert.AreEqual("Joe Cool is a remarkable student.", student.GetDescription());
        }


        // Test - Change the first name
        [TestMethod]
        public void ChangeStudentFirstName()
        {
            Student student = new Student("Joe", "Cool");
            student.SetFirstName("Bill");
            Assert.AreEqual("Bill", student.FirstName);
        }

        // Test - Change the last name
        [TestMethod]
        public void ChangeStudentLastName()
        {
            Student student = new CSStudent("Joe", "Cool");
            student.SetLastName("NotSoCool");
            Assert.AreEqual("NotSoCool", student.LastName);
        }

        // Test - Read student names from a file
        [TestMethod]
        public void ReadStudentNamesFromFile()
        {
            // Prepare test by writing names to a file
            string filename = "testStudentNames.txt";
            CreateTestFile(filename);

            // Read the names back into an array of strings (one string per line)
            string[] lines = File.ReadAllLines(filename);

            // Create an array of Students.
            Student[] students = new Student[lines.Length];

            // Allocate an instance of class student for each array entry.
            for (int i = 0; i < students.Length; i++)
            {
                students[i] = new Student();
            }

            // First and Last names are separated by a space or tab
            char[] delimiters = new char[2];
            delimiters[0] = ' ';
            delimiters[1] = '\t';

            // for each student, set the FirstName and LastName
            for (int i = 0; i < students.Length; i++)
            {
                // Split the line int an array of strings.
                // 
                string[] names = lines[i].Split(delimiters, 2);
                students[i].SetFirstName(names[0]);
                students[i].SetLastName(names[1]);
            }


            Assert.AreEqual("Joe", students[0].FirstName);
            Assert.AreEqual("Cool", students[0].LastName);
            Assert.AreEqual("Jane", students[1].FirstName);
            Assert.AreEqual("VeryCool", students[1].LastName);
        }

        private static void CreateTestFile(string filename)
        {
            DeleteTestFile(filename);
            StreamWriter testFile = new StreamWriter(filename);
            testFile.WriteLine("Joe Cool");
            testFile.WriteLine("Jane VeryCool");
            testFile.Close();
        }

        private static void DeleteTestFile(string filename)
        {
            FileInfo fileInfo = new FileInfo(filename);
            if (fileInfo.Exists)
            {
                fileInfo.Delete();
            }
        }


        // Test - Put a student in a class
        [TestMethod]
        public void AddStudentToClass()
        {
            Student joeCool = new Student("Joe", "Cool");
            Class myClass = new Class();
            myClass.Add(joeCool);
            Assert.AreEqual(1, myClass.GetNStudents());
        }

        // Test - See if two students are equal
        [TestMethod]
        public void StudentEquality()
        {
            Student joeCool = new Student("Joe", "Cool");
            Student joeCoolClone = new Student("Joe", "Cool");
            Student notJoe = new Student("Luke", "Warm");
            Assert.AreEqual(0, joeCool.CompareTo(joeCoolClone));
            Assert.AreNotEqual(0, joeCool.CompareTo(notJoe));
        }

        // Test - Make a copy of student.
        [TestMethod]
        public void CopyStudent()
        {
            Student joeCool = new Student("Joe", "Cool");
            Student copyOfJoe = new Student(joeCool);
            Assert.AreEqual(0, joeCool.CompareTo(copyOfJoe));
        }

        // Test - Make an assignment to student.
        [TestMethod]
        public void AssignmentOfStudent()
        {
            Student joeCool = new Student("Joe", "Cool");
            Student anotherStudent = new Student("Another", "Student");
            anotherStudent = joeCool;
            Assert.AreEqual(0, joeCool.CompareTo(anotherStudent));
        }

        // Test - See if student added to class is student retrieved
        [TestMethod]
        public void BracketOperatorForClass()
        {
            Student joeCool = new Student("Joe", "Cool");
            Class myClass = new Class();
            myClass.Add(joeCool);
            Assert.AreEqual(0, myClass[0].CompareTo(joeCool));
        }

        [TestMethod]
        public void ReadStudentFromStream()
        {
            string filename = "testStudentNames.txt";
            CreateTestFile(filename);

            StreamReader reader = new StreamReader(filename);
            Student readStudent = new Student();
            readStudent.ReadFromStream(reader);
            reader.Close();
            Assert.AreEqual("Joe", readStudent.FirstName);
            Assert.AreEqual("Cool", readStudent.LastName);
        }

        // Test - Read all the student names from a file into a class
        [TestMethod]
        public void ReadClassFromTextFile()
        {
            string filename = "testStudentNames.txt";
            CreateTestFile(filename);

            StreamReader reader = new StreamReader(filename);
            Class myClass = new Class();
            myClass.ReadTextFile(reader);
            reader.Close();

            Assert.AreEqual(0,
                myClass[0].CompareTo(new Student("Joe", "Cool")));
            Assert.AreEqual(0,
                myClass[1].CompareTo(new Student("Jane", "VeryCool")));

        }

		// Test Date class
		[TestMethod]
		public void CreateSpecifiedDate()
		{
			// Send the constructor month, day, year
			Date myDate = new Date(1, 2, 2018);
			Assert.AreEqual(2, myDate.Day);
			Assert.AreEqual(1, myDate.Month);
			Assert.AreEqual(2018, myDate.Year);
		}

		// Create an enumeration type for days inside the date class
		[TestMethod]
		public void DayEnums()
		{
			Assert.AreEqual(0, (int)Date.DayOfWeek.Sunday);
			Assert.AreEqual(1, (int)Date.DayOfWeek.Monday);
			Assert.AreEqual(2, (int)Date.DayOfWeek.Tuesday);
			Assert.AreEqual(3, (int)Date.DayOfWeek.Wednesday);
			Assert.AreEqual(4, (int)Date.DayOfWeek.Thursday);
			Assert.AreEqual(5, (int)Date.DayOfWeek.Friday);
			Assert.AreEqual(6, (int)Date.DayOfWeek.Saturday);
		}

		// Find the name of a day of the week
		[TestMethod]
		public void GetDayNameFromEnum()
		{
		   Assert.AreEqual("Sunday",
		       Date.DayOfWeek.Sunday.ToString());
		   Assert.AreEqual("Monday",
		       Date.DayOfWeek.Monday.ToString());
		   Assert.AreEqual("Tuesday",
		       Date.DayOfWeek.Tuesday.ToString());
		   Assert.AreEqual("Wednesday",
		       Date.DayOfWeek.Wednesday.ToString());
		   Assert.AreEqual("Thursday",
		       Date.DayOfWeek.Thursday.ToString());
		   Assert.AreEqual("Friday",
		       Date.DayOfWeek.Friday.ToString());
		   Assert.AreEqual("Saturday",
		       Date.DayOfWeek.Saturday.ToString());
		}

		// Create enumeration type for Months
		[TestMethod]
		public void MonthEnums()
		{
		   Assert.AreEqual(1, (int)Date.MonthName.January);
		   Assert.AreEqual(2, (int)Date.MonthName.February);
		   Assert.AreEqual(3, (int)Date.MonthName.March);
		   Assert.AreEqual(4, (int)Date.MonthName.April);
		   Assert.AreEqual(5, (int)Date.MonthName.May);
		   Assert.AreEqual(6, (int)Date.MonthName.June);
		   Assert.AreEqual(7, (int)Date.MonthName.July);
		   Assert.AreEqual(8, (int)Date.MonthName.August);
		   Assert.AreEqual(9, (int)Date.MonthName.September);
		   Assert.AreEqual(10, (int)Date.MonthName.October);
		   Assert.AreEqual(11, (int)Date.MonthName.November);
		   Assert.AreEqual(12, (int)Date.MonthName.December);
		}

		// Get a month name from the enum
		[TestMethod]
		public void MonthNameFromEnum()
		{
		   Assert.AreEqual("January", Date.MonthName.January.ToString());
		   Assert.AreEqual("February", Date.MonthName.February.ToString());
		   Assert.AreEqual("March", Date.MonthName.March.ToString());
		   Assert.AreEqual("April", Date.MonthName.April.ToString());
		   Assert.AreEqual("May", Date.MonthName.May.ToString());
		   Assert.AreEqual("June", Date.MonthName.June.ToString());
		   Assert.AreEqual("July", Date.MonthName.July.ToString());
		   Assert.AreEqual("August", Date.MonthName.August.ToString());
		   Assert.AreEqual("September", Date.MonthName.September.ToString());
		   Assert.AreEqual("October", Date.MonthName.October.ToString());
		   Assert.AreEqual("November", Date.MonthName.November.ToString());
		   Assert.AreEqual("December", Date.MonthName.December.ToString());
		}


		// Find out if a year is a leap year.
		// Leap years are years that are divisible by 4, unless they are also divisible by 100, unless
		// they are also divisible by 400.
		[TestMethod]
		public void DetermineIfYearIsLeapYear()
		{
		   Assert.IsTrue(Date.isLeapYear(2016));
		   Assert.IsFalse(Date.isLeapYear(2100));
		   Assert.IsTrue(Date.isLeapYear(2000));
		   Assert.IsFalse(Date.isLeapYear(2015));
		}

		// Count leap years since 1600
		[TestMethod]
		public void CountLeapYearFrom1600toBeforeSpecifiedYear()
		{
			Assert.AreEqual(0,
				Date.NLeapYearsFrom1600toBefore(1600));
			Assert.AreEqual(2,
				Date.NLeapYearsFrom1600toBefore(1605));
			Assert.AreEqual(25,
				Date.NLeapYearsFrom1600toBefore(1700));
			Assert.AreEqual(25,
				Date.NLeapYearsFrom1600toBefore(1701));
		}


		// Find the start day for each month
		[TestMethod]
		public void TestMonthOffsetEnums()
		{
			Assert.AreEqual(0, (int)Date.Offset.January);
			Assert.AreEqual(3, (int)Date.Offset.February);
			Assert.AreEqual(3, (int)Date.Offset.March);
			Assert.AreEqual(6, (int)Date.Offset.April);
			Assert.AreEqual(1, (int)Date.Offset.May);
			Assert.AreEqual(4, (int)Date.Offset.June);
			Assert.AreEqual(6, (int)Date.Offset.July);
			Assert.AreEqual(2, (int)Date.Offset.August);
			Assert.AreEqual(5, (int)Date.Offset.September);
			Assert.AreEqual(0, (int)Date.Offset.October);
			Assert.AreEqual(3, (int)Date.Offset.November);
			Assert.AreEqual(5, (int)Date.Offset.December);
		}

		// Find the month offset for a given date
		[TestMethod]
		public void GetMonthOffsetForSpecifiedDate()
		{
			Date janDate = new Date(1, 1, 2014);
			Assert.AreEqual(Date.Offset.January, janDate.GetMonthOffset());

			Date junDate = new Date(6, 16, 2014);
			Assert.AreEqual(Date.Offset.June, junDate.GetMonthOffset());

			Date decDate = new Date(12, 12, 2014);
			Assert.AreEqual(Date.Offset.December, decDate.GetMonthOffset());
		}

		// January 1, 1600 was a Saturday (day number 6)
		// Any date since then can be calculated as
		//   (5 + DayOfMonth + NumberOfYearsSince1600 + NumberOfLeapYearsSince1600ToThatYear + MonthOffset) % 7
		// If the year is a leap year and it is March 1st or later add 1 and take mod 7 again

		// Find the day of the week
		[TestMethod]
		public void ComputeDayOfTheWeek()
		{
			Date today = new Date(8, 27, 2018);
			Assert.AreEqual(Date.DayOfWeek.Monday, today.DayName);

			Date nextMonth = new Date(9, 27, 2018);
			Assert.AreEqual(Date.DayOfWeek.Thursday, nextMonth.DayName);

			Date pearlHarborYear = new Date(1, 25, 1941);
			Assert.AreEqual(Date.DayOfWeek.Saturday, pearlHarborYear.DayName);

			Date pearlHarborDay = new Date(12, 7, 1941);
			Assert.AreEqual(Date.DayOfWeek.Sunday, pearlHarborDay.DayName);

			Date blackTuesday = new Date(10, 29, 1929);
			Assert.AreEqual(Date.DayOfWeek.Tuesday, blackTuesday.DayName);

			Date leapYearDayBeforeFeb = new Date(1, 1, 2016);
			Assert.AreEqual(Date.DayOfWeek.Friday,
				leapYearDayBeforeFeb.DayName);

			Date leapYearDayAfterFeb = new Date(7, 4, 2016);
			Assert.AreEqual(Date.DayOfWeek.Monday,
				leapYearDayAfterFeb.DayName);

			Date bigParty = new Date(12, 31, 2999);
			Assert.AreEqual(Date.DayOfWeek.Tuesday,
				bigParty.DayName);
		}

		// Find the number of days in each month
		[TestMethod]
		public void FindDaysPerMonth()
		{
			Assert.AreEqual(31, Date.GetDaysPerMonth(1, 2014));
			Assert.AreEqual(28, Date.GetDaysPerMonth(2, 2014));
			Assert.AreEqual(31, Date.GetDaysPerMonth(3, 2014));
			Assert.AreEqual(30, Date.GetDaysPerMonth(4, 2014));
			Assert.AreEqual(31, Date.GetDaysPerMonth(5, 2014));
			Assert.AreEqual(30, Date.GetDaysPerMonth(6, 2014));
			Assert.AreEqual(31, Date.GetDaysPerMonth(7, 2014));
			Assert.AreEqual(31, Date.GetDaysPerMonth(8, 2014));
			Assert.AreEqual(30, Date.GetDaysPerMonth(9, 2014));
			Assert.AreEqual(31, Date.GetDaysPerMonth(10, 2014));
			Assert.AreEqual(30, Date.GetDaysPerMonth(11, 2014));
			Assert.AreEqual(31, Date.GetDaysPerMonth(12, 2014));
			Assert.AreEqual(29, Date.GetDaysPerMonth(2, 2016));
			Assert.AreEqual(31, Date.GetDaysPerMonth(3, 2016));
		}

		// Increment a date
		[TestMethod]
		public void IncrementADate()
		{
			Date today = new Date(8, 27, 2018);
			++today;
			Assert.AreEqual(0, today.CompareTo(new Date(8, 28, 2018)));

			Date toNextMonth = new Date(1, 31, 2014);
			++toNextMonth;
			Assert.AreEqual(0, toNextMonth.CompareTo(new Date(2, 1, 2014)));

			Date toNextYear = new Date(12, 31, 2014);
			++toNextYear;
			Assert.AreEqual(0, toNextYear.CompareTo(new Date(1, 1, 2015)));
		}


		[TestMethod]
		public void makeARotatingVector()
		{
			RotatingVector<string> myVector = new RotatingVector<string>();
			myVector.Add("a");
			myVector.Add("b");
			myVector.Add("c");

			Assert.AreEqual("a", myVector[0]);
			Assert.AreEqual("b", myVector[1]);
			Assert.AreEqual("c", myVector[2]);
		}

		//Rotate a vector
		[TestMethod]
		public void RotateAVector()
		{
			RotatingVector<string> myVector = new RotatingVector<string>();
			myVector.Add("a");
			myVector.Add("b");
			myVector.Add("c");

			myVector.Rotate();

			Assert.AreEqual("b", myVector[0]);
			Assert.AreEqual("c", myVector[1]);
			Assert.AreEqual("a", myVector[2]);
		}

		// Make an instance of class Pairing
		[TestMethod]
		public void TestNameWrong()
		{
			Student firstStudent = new Student("First", "Student");
			Student secondStudent = new Student("Second", "Student");
			Date pairingDate = new Date(8, 27, 2018);

			Pairing pairing = new Pairing(firstStudent, secondStudent, pairingDate);

			Assert.AreEqual(0, firstStudent.CompareTo(pairing.FirstStudent));
			Assert.AreEqual(0, secondStudent.CompareTo(pairing.SecondStudent));
			Assert.AreEqual(0, pairingDate.CompareTo(pairing.Date));
		}

		// Write code to compare two pairings.  Pairings are equal if they have 
		// the same date, even if firstStudent and secondStudent are switched.
		[TestMethod]
		public void ComparePairings()
		{
			Student firstStudent = new Student("First", "Student");
			Student secondStudent = new Student("Second", "Student");
			Date pairingDate = new Date(1, 26, 2014);
			Pairing firstPairing =
				new Pairing(firstStudent, secondStudent, pairingDate);
			Pairing secondPairing =
				new Pairing(secondStudent, firstStudent, pairingDate);

			Assert.AreEqual(0, firstPairing.CompareTo(firstPairing));
			Assert.AreEqual(0, secondPairing.CompareTo(firstPairing));
			Assert.AreEqual(0, firstPairing.CompareTo(secondPairing));
		}

		// Determine the number of days since the start of the year
		[TestMethod]
		public void ComputeDaysSinceStartOfYear()
		{
			Date firstOfYear = new Date(1, 1, 2018);
			Assert.AreEqual(0, firstOfYear.DaysSinceStartOfYear());

			Date another = new Date(1, 3, 2018);
			Assert.AreEqual(2, another.DaysSinceStartOfYear());

			Date noLeapYear = new Date(12, 31, 2018);
			Assert.AreEqual(364, noLeapYear.DaysSinceStartOfYear());

			Date leapYear = new Date(12, 31, 2020);
			Assert.AreEqual(365, leapYear.DaysSinceStartOfYear());
		}

		// Determine if one date is earlier than another 
		[TestMethod]
		public void DateOperatorLTGT()
		{
			Date earlier = new Date(1, 25, 2018);
			Date later = new Date(1, 26, 2018);

			Assert.IsTrue(earlier < later);
			Assert.IsFalse(earlier > later);
		}


		// Disallow days particular days of the week
		[TestMethod]
		public void DisallowDaysOfTheWeek()
		{
			Date mwfOnly = new Date(8, 27, 2018);
			List<Date.DayOfWeek> disallowedDays = new List<Date.DayOfWeek>();
			disallowedDays.Add(Date.DayOfWeek.Saturday);
			disallowedDays.Add(Date.DayOfWeek.Sunday);
			disallowedDays.Add(Date.DayOfWeek.Tuesday);
			disallowedDays.Add(Date.DayOfWeek.Thursday);
			mwfOnly.RestrictDayOfWeek(disallowedDays);

			Assert.AreEqual(0, mwfOnly.CompareTo(new Date(8, 27, 2018)));
			++mwfOnly;
			Assert.AreEqual(0, mwfOnly.CompareTo(new Date(8, 29, 2018)));
			++mwfOnly;
			Assert.AreEqual(0, mwfOnly.CompareTo(new Date(8, 31, 2018)));
			++mwfOnly;
			Assert.AreEqual(0, mwfOnly.CompareTo(new Date(9, 3, 2018)));
		}

		// Disallow specific dates
		[TestMethod]
		public void DisallowDates()
		{
			// No class on the 10/12
			Date no12th = new Date(10, 11, 2018);
			List<Date> disallowedDates = new List<Date>();
			disallowedDates.Add(new Date(10, 12, 2018));
			no12th.RestrictDates(disallowedDates);
			++no12th;
			Assert.AreEqual(0, no12th.CompareTo(new Date(10, 13, 2018)));
		}

		// Get date suffix
		[TestMethod]
		public void GetDateSuffix()
		{
			Date first = new Date(1, 1, 2018);
			Date twentyFirst = new Date(1, 21, 2018);
			Date thirtyFirst = new Date(1, 31, 2018);
			Date second = new Date(1, 2, 2018);
			Date twentySecond = new Date(1, 22, 2018);
			Date third = new Date(1, 3, 2018);
			Date twentythird = new Date(1, 23, 2018);
			Date forth = new Date(1, 4, 2018);
			Date eleventh = new Date(1, 11, 2018);
			Date thirteenth = new Date(1, 13, 2018);

			Assert.AreEqual("st", first.GetSuffix());
			Assert.AreEqual("st", twentyFirst.GetSuffix());
			Assert.AreEqual("st", thirtyFirst.GetSuffix());

			Assert.AreEqual("nd", second.GetSuffix());
			Assert.AreEqual("nd", twentySecond.GetSuffix());

			Assert.AreEqual("rd", third.GetSuffix());
			Assert.AreEqual("rd", twentythird.GetSuffix());

			Assert.AreEqual("th", forth.GetSuffix());
			Assert.AreEqual("th", eleventh.GetSuffix());
			Assert.AreEqual("th", thirteenth.GetSuffix());
		}

		//// PairMaker is a template class that will be used to create all pairing.
		//// Make a PairMaker instance --
		////   The constructor takes a vector and splits it into two separate vectors
		////   The input vector
		////   0
		////   1
		////   2
		////   3
		////   4
		////   5
		////   6
		////   7
		////   8
		////   9
		////   This causes the creation of two vectors internal to the PairMaker class
		////   "leftSide" will be:
		////    0
		////    1
		////    2
		////    3
		////    4
		////   and "rightSide" will be:
		////    9
		////    8
		////    7
		////    6
		////    5

		[TestMethod]
		public void createPairMaker()
		{
			// Create List with integers 0-9
			List<int> intList = new List<int>();
			const int POPULATION_SIZE = 10;
			for (int i = 0; i < POPULATION_SIZE; i++) {
				intList.Add(i);
			}

			// Create an instance of pairMaker
			PairMaker<int> pairMaker = new PairMaker<int>(intList);


			// Get and check the leftSide array
			Assert.AreEqual(POPULATION_SIZE / 2, pairMaker.LeftSide.Count);
			for (int i = 0; i < pairMaker.LeftSide.Count; i++) {
				Assert.AreEqual(intList[i], pairMaker.LeftSide[i]);
			}

			// Get and check the rightSide array
			for (int i = 0; i < pairMaker.RightSide.Count; i++) {
				Assert.AreEqual(intList[intList.Count - 1 - i],
					pairMaker.RightSide[i]);
			}
		}

		// getPairs returns pairs such that the ith element in leftSide
		// is paired with the ith element in rightSide
		[TestMethod]
		public void GetPairs()
		{
			// Make a list with values 0-9
			List<int> intVector = new List<int>();
			const int POPULATION_SIZE = 10;
			for (int i = 0; i < POPULATION_SIZE; i++) {
				intVector.Add(i);
			}
			PairMaker<int> pairMaker = new PairMaker<int>(intVector);

			// Get all the pairs
			List<Tuple<int, int>> matches = pairMaker.GetPairs();

			// Check the pairs
			for (int i = 0; i < POPULATION_SIZE / 2; i++) {
				Assert.AreEqual(matches[i].Item1, i);
				Assert.AreEqual(matches[i].Item2, POPULATION_SIZE - 1 - i);
			}
		}

		//// Rotate the vectors counter-clockwise excluding the first element
		////  in leftSide.  In the resulting rotation
		////   -- The last element in leftSide becomes the last element in rightSide
		////   -- All the elements except the 1st element in left side move down by one
		////   -- The top element in right side becomes the second element in left side.
		////   -- All the elements in rightSide move up by one position
		////
		////  Before rotation: 
		////
		////   leftSide      rightSide
		////     0             9
		////     1             8
		////     2             7
		////     3             6
		////     4             5

		////  After rotation: 
		////
		////   leftSide      rightSide
		////     0             8
		////     9             7 
		////     1             6
		////     2             5
		////     3             4

		[TestMethod]
		public void RotatePairs()
		{
			// Make a List with values 0-9
			List<int> intVector = new List<int>();
			const int POPULATION_SIZE = 10;
			for (int i = 0; i < POPULATION_SIZE; i++) {
				intVector.Add(i);
			}

			PairMaker<int> pairMaker = new PairMaker<int>(intVector);

			// Test the copy constructor
			PairMaker<int> testPair = new PairMaker<int>(pairMaker);

			// User the ++ operator to cause a rotation.
			testPair.Rotate();

			// Check the results

			Assert.AreEqual(0, testPair.LeftSide[0]);
			Assert.AreEqual(9, testPair.LeftSide[1]);
			Assert.AreEqual(1, testPair.LeftSide[2]);
			Assert.AreEqual(2, testPair.LeftSide[3]);
			Assert.AreEqual(3, testPair.LeftSide[4]);

			Assert.AreEqual(8, testPair.RightSide[0]);
			Assert.AreEqual(7, testPair.RightSide[1]);
			Assert.AreEqual(6, testPair.RightSide[2]);
			Assert.AreEqual(5, testPair.RightSide[3]);
			Assert.AreEqual(4, testPair.RightSide[4]);
		}

		// Generate the pairs for one individual 
		// Test the algorithm on a number of vector sizes.
		[TestMethod]
		public void GeneratePairsForOneIndividual()
		{
			const int MIN_PAIRING_SIZE = 4;
			const int MAX_PAIRING_SIZE = 10;
			for (int vectorSize = MIN_PAIRING_SIZE; vectorSize <= MAX_PAIRING_SIZE; vectorSize += 2) {
				List<int> bigVector = new List<int>();
				for (int i = 0; i < vectorSize; i++) {
					bigVector.Add(i);
				}

				PairMaker<int> matcher = new PairMaker<int>(bigVector);

				// Keep track of all the pairings that have been made

				// Create a matrix with an element for each pairing
				// Initialize to all zeros.
				List<List<int>> matchRecord = new List<List<int>>();
				for (int i = 0; i < bigVector.Count; i++) {
					matchRecord.Add(new List<int>());
					for (int j = 0; j < bigVector.Count; j++) {
						matchRecord[i].Add(0);
					}
				}

				// Generate all the pairs
				for (int i = 0; i < bigVector.Count - 1; i++) {
					List<Tuple<int, int>> match = matcher.GetPairs();

					for (int j = 0; j < bigVector.Count / 2; j++) {
						Assert.IsTrue(matchRecord[match[j].Item1][match[j].Item2] == 0);
						Assert.IsTrue(matchRecord[match[j].Item2][match[j].Item1] == 0);

						matchRecord[match[j].Item1][match[j].Item2] = i + 1;
						matchRecord[match[j].Item2][match[j].Item1] = i + 1;

						Assert.IsTrue(matchRecord[match[j].Item1][match[j].Item2] == i + 1);
						Assert.IsTrue(matchRecord[match[j].Item2][match[j].Item1] == i + 1);
					}
					matcher.Rotate();
				}
				Console.WriteLine("Correct for n = {0}", vectorSize);
			}
		}

		// Get the matches for a single person
		[TestMethod]
		public void GeneratePairsForFounders()
		{
			List<string> founders = new List<string>();
			founders.Add("Knuth");
			founders.Add("Sedgewick");
			founders.Add("Turing");
			founders.Add("Dijkstra");

			PairMaker<string> founderMatcher = new PairMaker<string>(founders);

			Assert.AreEqual("Dijkstra", founderMatcher.GetMatch("Knuth"));
			founderMatcher.Rotate();
			Assert.AreEqual("Turing", founderMatcher.GetMatch("Knuth"));
			founderMatcher.Rotate();
			Assert.AreEqual("Sedgewick", founderMatcher.GetMatch("Knuth"));
			founderMatcher.Rotate();

			Assert.AreEqual("Knuth", founderMatcher.GetMatch("Dijkstra"));
			founderMatcher.Rotate();
			Assert.AreEqual("Sedgewick", founderMatcher.GetMatch("Dijkstra"));
			founderMatcher.Rotate();
			Assert.AreEqual("Turing", founderMatcher.GetMatch("Dijkstra"));
			founderMatcher.Rotate();

			Assert.AreEqual("Turing", founderMatcher.GetMatch("Sedgewick"));
			founderMatcher.Rotate();
			Assert.AreEqual("Dijkstra", founderMatcher.GetMatch("Sedgewick"));
			founderMatcher.Rotate();
			Assert.AreEqual("Knuth", founderMatcher.GetMatch("Sedgewick"));
			founderMatcher.Rotate();
		}
	}
}
