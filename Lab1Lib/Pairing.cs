﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1Lib
{
	public class Pairing : IComparable
	{
		public Pairing(Student first, Student second, Date date)
		{
			FirstStudent = new Student(first);
			SecondStudent = new Student(second);
			Date = new Date(date);
		}

		public int CompareTo(object other)
		{
			Pairing rhs = (Pairing)other;
			int ret = 0;
			if (Date != rhs.Date) {
				ret = Date.CompareTo(rhs.Date);
			} else if (FirstStudent == rhs.FirstStudent) {
				ret = SecondStudent.CompareTo(rhs.SecondStudent);
			} else if (FirstStudent == rhs.SecondStudent) {
				ret = SecondStudent.CompareTo(rhs.SecondStudent);
			} else if (SecondStudent == rhs.FirstStudent) {
				ret = FirstStudent.CompareTo(rhs.SecondStudent);
			} else if (SecondStudent == rhs.SecondStudent) {
				ret = FirstStudent.CompareTo(rhs.FirstStudent);
			} else {
				ret = FirstStudent.CompareTo(rhs.FirstStudent);
			}
			return ret;
		}

		public Student FirstStudent { get; private set; }
		public Student SecondStudent { get; private set; }
		public Date Date { get; private set; }
	}
}
