﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab1Lib
{
    public class Class
    {
        public Class(int numStudents = 0)
        {
            //NStudents = numStudents;
            students = new List<Student>(numStudents+1);
            for (int i = 0; i < numStudents; i++)
            {
                students.Add(new Student());
            }
        }

        public void Add(Student student)
        {
            students.Add(student);
        }

        public Student this[int i] { get => students[i];
			private set => students[i] = value; }

		public void ReadTextFile(StreamReader reader)
		{
			string line;
			while ((line = reader.ReadLine()) != null) {
				string[] tok = line.Split(' ');
				Add(new Student(tok[0]));
				if (tok.Length >= 2) this[NStudents - 1].SetLastName(tok[1]);
			}
		}

        public int GetNStudents()
        {
            return NStudents;
        }
        public int NStudents { get => students.Count; }
        public List<Student> students { get; private set; }
    }
}
