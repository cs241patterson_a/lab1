﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1Lib
{
	public class PairMaker<T>
	{
		public PairMaker() { }

		public PairMaker(List<T> fullList)
		{
			for (int i = 0, j = fullList.Count - 1; j >= i; i++, j--) {
				LeftSide.Add(fullList[i]);
				RightSide.Add(fullList[j]);
			}
		}

		public PairMaker(PairMaker<T> other)
		{
			this.LeftSide = new List<T>(other.LeftSide);
			this.RightSide = new List<T>(other.RightSide);
		}

		public void Rotate()
		{
			List<T> tmp = new List<T>();
			for (int i = 1; i < LeftSide.Count; i++) {
				tmp.Add(LeftSide[i]);
			}
			for (int i = RightSide.Count-1; i >= 0; i--) {
				tmp.Add(RightSide[i]);
			}

			T old = tmp[0];
			for (int i = 1; i < tmp.Count; i++) {
				T mov = old;
				old = tmp[i];
				tmp[i] = mov;
			}
			tmp[0] = old;

			tmp.Insert(0, LeftSide[0]);
			var reshuffled = new PairMaker<T>(tmp);
			this.LeftSide = new List<T>(reshuffled.LeftSide);
			this.RightSide = new List<T>(reshuffled.RightSide);
		}

		// Getters
		public List<Tuple<T, T>> GetPairs()
		{
			var ret = new List<Tuple<T, T>>();

			for (int i = 0; i < LeftSide.Count; i++) {
				ret.Add(new Tuple<T, T>(LeftSide[i], RightSide[i]));
			}

			return ret;
		}

		public T GetMatch(T thing)
		{
			T ret = thing;
			if (LeftSide.Contains(thing)) {
				ret = RightSide[LeftSide.IndexOf(thing)];
			} else if (RightSide.Contains(thing)) {
				ret = LeftSide[RightSide.IndexOf(thing)];
			}
			return ret;
		}

		public List<T> LeftSide { get; private set; } = new List<T>();
		public List<T> RightSide { get; private set; } = new List<T>();

		//private List<Tuple<T, T>> PairList = new List<Tuple<T, T>();
	}
}
