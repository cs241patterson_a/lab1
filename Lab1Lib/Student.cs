﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab1Lib
{
    public class Student : IComparable
    {
        public int CompareTo(object obj)
        {
            Student other = (Student)obj;
            //If smaller, -1
            //If bigger, 1
            //If equal, 0
            int ret = 0;
            if (LastName != other.LastName)
            {
                ret = LastName.CompareTo(other.LastName);
            }
            else if (FirstName != other.FirstName)
            {
                ret = FirstName.CompareTo(other.FirstName);
            }
            return ret;
        }

        public Student(string firstName = "Unknown", string lastName = "Unknown")
        {
            FirstName = firstName;
            LastName = lastName;
        }
        public Student(Student other)
        {
            FirstName = other.FirstName;
            LastName = other.LastName;
        }

        virtual public string GetDescription()
        {
            return FirstName + " " + LastName + " is a student.";
        }

        public void SetFirstName(string newFirstName)
        {
            FirstName = newFirstName;
        }
        public void SetLastName(string newLastName)
        {
            LastName = newLastName;
        }

        public void ReadFromStream(StreamReader reader)
        {
            string line;
			line = reader.ReadLine();
			string[] tokenized = line.Split(' ');
			SetFirstName(tokenized[0]);
			if (tokenized.Length >= 2)
				SetLastName(tokenized[1]);
        }

        //This is bad internationalization...
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
    }
}
