﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1Lib
{
	public class Date : IComparable, IEquatable<Date>
	{
		public enum DayOfWeek { Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday }
		public enum MonthName { January = 1, February, March, April, May, June, July, August, September, October, November, December }
		public enum Offset { January = 0, February = 3, March = 3, April = 6, May = 1, June = 4,
			July = 6, August = 2, September = 5, October = 0, November = 3, December = 5, NaM = -1 }

		public Date(int month = 01, int day = 01, int year = 1970)
		{
			this.Month = month;
			this.Day = day;
			this.Year = year;

			normalize();
		}
		public Date(Date other)
		{
			Day = other.Day;
			Month = other.Month;
			Year = other.Year;
		}

		public static bool isLeapYear(int yearNum) {
			bool ret = false;
			if ((yearNum % 4) == 0) {
				if ((yearNum % 100) != 0) {
					ret = true;
				} else if ((yearNum % 400) == 0) {
					ret = true;
				}
			}
			return ret;
		}

		public bool isLeapYear()
		{
			return Date.isLeapYear(this.Year);
		}

		public static int NLeapYearsFrom1600toBefore(int year)
		{
			int count = 0;
			for (int i = 1600; i < year; i++) {
				if (isLeapYear(i)) {
					count++;
				}
			}
			return count;

		}

		public Date Copy(Date rhs)
		{
			this.Day = rhs.Day;
			this.Month = rhs.Month;
			this.Year = rhs.Year;
			this.normalize();
			return this;
		}

		public void RestrictDayOfWeek(List<DayOfWeek> restricted)
		{
			RestrictedDaysOfWeek = new List<DayOfWeek>(restricted);
			normalize();
		}

		public void RestrictDates(List<Date> restricted)
		{
			RestrictedDates = new List<Date>(restricted);
			normalize();
		}

		private void normalize()
		{
			const int MO_PER_YR = 12;
			while (!isNormalized()) {
				if (Day < 1) {
					Month--;
					Day += GetDaysPerMonth(Month, Year);
				} else if (Day > GetDaysPerMonth(Month, Year)) {
					Day -= GetDaysPerMonth(Month, Year);
					Month++;
				}

				if (Month < 1) {
					Year--;
					Month += MO_PER_YR;
				} else if (Month > MO_PER_YR) {
					Month -= MO_PER_YR;
					Year++;
				}
			}
			if (RestrictedDaysOfWeek.Contains(this.DayName)) {
				Day++;
				normalize();
			}
			if (RestrictedDates.Contains(this)) {
				Day++;
				normalize();
			}
		}

		private bool isNormalized()
		{
			bool normalized = true;
			if ((Month > 12) || (Month < 1)) {
				normalized = false;
			} else if ((Day > Date.GetDaysPerMonth(Month, Year)) || (Day < 1)) {
				normalized = false;
			}

			return normalized;
		}

		//Getters
		private DayOfWeek GetDayOfWeek()
		{
			int yearsSince1600 = Year - 1600;
			const int OFFSET_1600 = 5;
			const int WEEK_MOD = 7;
			int leapYearAfterFeb = 0;
			if (isLeapYear() && (Month > (int)Date.MonthName.February))
				leapYearAfterFeb = 1;
			return (DayOfWeek)((yearsSince1600 + NLeapYearsFrom1600toBefore(Year) + leapYearAfterFeb
				+ Day + (int)GetMonthOffset() + OFFSET_1600) % WEEK_MOD);
		}

		public Offset GetMonthOffset()
		{
			Offset offset;
			switch ((MonthName)this.Month) {
				case Date.MonthName.January: offset = Offset.January; break;
				case Date.MonthName.February: offset = Offset.February; break;
				case Date.MonthName.March: offset = Offset.March; break;
				case Date.MonthName.April: offset = Offset.April; break;
				case Date.MonthName.May: offset = Offset.May; break;
				case Date.MonthName.June: offset = Offset.June; break;
				case Date.MonthName.July: offset = Offset.July; break;
				case Date.MonthName.August: offset = Offset.August; break;
				case Date.MonthName.September: offset = Offset.September; break;
				case Date.MonthName.October: offset = Offset.October; break;
				case Date.MonthName.November: offset = Offset.November; break;
				case Date.MonthName.December: offset = Offset.December; break;
				default: offset = Offset.NaM; break;
			}

			return offset;
		}

		public static int GetDaysPerMonth(int month, int year)
		{
			int days = -1;
			switch (month) {
				case (int)MonthName.January: days = 31; break;
				case (int)MonthName.February:
					if (isLeapYear(year))
						days = 29;
					else days = 28;
					break;
				case (int)MonthName.March: days = 31; break;
				case (int)MonthName.April: days = 30; break;
				case (int)MonthName.May: days = 31; break;
				case (int)MonthName.June: days = 30; break;
				case (int)MonthName.July: days = 31; break;
				case (int)MonthName.August: days = 31; break;
				case (int)MonthName.September: days = 30; break;
				case (int)MonthName.October: days = 31; break;
				case (int)MonthName.November: days = 30; break;
				case (int)MonthName.December: days = 31; break;
			}
			return days;
		}

		public string GetSuffix()
		{
			string ret;

			if ((10 < Day) && (Day < 20)) {
				ret = "th";
			} else switch (Day % 10) {
					case 1: ret = "st"; break;
					case 2: ret = "nd"; break;
					case 3: ret = "rd"; break;
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					case 0:
						ret = "th"; break;
					default: ret = "";  break;
				}

			return ret;
		}

		public int DaysSinceStartOfYear()
		{
			int ret = Day - 1;
			Day = 1;

			while (Month != 1) {
				Month--;
				ret += GetDaysPerMonth(Month, Year);
			}

			return ret;
		}

		//Interfaces
		public int CompareTo(object obj)
		{
			Date rhs = (Date)obj;
			int notBool;
			if (this.Year != rhs.Year) {
				notBool = this.Year.CompareTo(rhs.Year);
			} else if (this.Month != rhs.Month) {
				notBool = this.Month.CompareTo(rhs.Month);
			} else {
				notBool = this.Day.CompareTo(rhs.Day);
			}
			return notBool;
		}

		public bool Equals(Date other)
		{
			return CompareTo(other) == 0;
		}

		//Operators
		public static Date operator++(Date foo)
		{
			foo.Day++;
			foo.normalize();
			return foo;
		}

		public static Date operator+(Date foo, int num)
		{
			return new Date(foo.Month, foo.Day + num, foo.Year);
		}
		public static Date operator-(Date foo, int num)
		{
			return new Date(foo.Month, foo.Day - num, foo.Year);
		}

		public static bool operator>(Date lhs, Date rhs)
		{
			return lhs.CompareTo(rhs) > 0;
		}
		public static bool operator<(Date lhs, Date rhs)
		{
			return lhs.CompareTo(rhs) < 0;
		}

		//Properties
		public DayOfWeek DayName { get => GetDayOfWeek(); }
		public int Day { get; private set; }
		public int Month { get ; private set; }
		public int Year { get; private set; }
		private List<Date.DayOfWeek> RestrictedDaysOfWeek = new List<Date.DayOfWeek>();
		private List<Date> RestrictedDates = new List<Date>();
	}
}
