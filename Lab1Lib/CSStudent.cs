﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1Lib
{
    public class CSStudent: Student
    {
        public CSStudent(string firstName = "Unknown", string lastName = "Unknown") : base (firstName, lastName) {}

        override public string GetDescription()
        {
            return FirstName + " " + LastName + " is a remarkable student.";
        }
    }
}
