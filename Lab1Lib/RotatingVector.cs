﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1Lib
{
	public class RotatingVector<T> //: List<T>
	{
		public void Add(T thing)
		{
			List.Add(thing);
		}

		public void Rotate(int rot = 1)
		{
			Offset += rot;
		}

		public T this[int i] { get => List[(i + Offset) % List.Count]; }
		private int Offset;
		private List<T> List = new List<T>();
	}
}
